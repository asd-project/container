//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/concepts.h>
#include <meta/tuple.h>
#include <meta/types/nth.h>
#include <meta/types/find.h>

//---------------------------------------------------------------------------

namespace asd
{
    // std::tuple is not a standard-layout class, so it cannot be used to store primitive data which can be easily mapped to flat array without UB.
    // plain_tuple solves this issue by using recursive composition instead of inheritance

    template <class... Rest>
    struct plain_tuple;

    template <class T>
    constexpr bool is_const_rvalue_v = std::is_same_v<T &&, const T &>;

    namespace detail
    {
        template <size_t Index, class Tuple>
        constexpr auto && get(Tuple && t) noexcept {
            if constexpr (Index == 0) {
                return std::forward<Tuple>(t).head;
            } else {
                return ::asd::detail::get<Index - 1>(std::forward<Tuple>(t).tail);
            }
        }
    }

    template <class T, class... Rest>
    struct plain_tuple<T, Rest...>
    {
        constexpr plain_tuple(const T & head, const Rest & ... rest) :
            head(head), tail(rest...) {}

        constexpr plain_tuple(const T & head, Rest && ... rest) requires (... || !is_const_rvalue_v<Rest>) :
            head(head), tail(std::move(rest)...) {}

        constexpr plain_tuple(T && head, const Rest & ... rest) requires (!is_const_rvalue_v<T>) :
            head(std::move(head)), tail(rest...) {}

        constexpr plain_tuple(T && head, Rest && ... rest) requires (!is_const_rvalue_v<T> && (... || !is_const_rvalue_v<Rest>)) :
            head(std::move(head)), tail(std::move(rest)...) {}

        template <size_t Index> requires(Index <= sizeof...(Rest))
        decltype(auto) operator[](std::integral_constant<size_t, Index>) {
            return ::asd::detail::get<Index>(*this);
        }

        template <size_t Index> requires(Index <= sizeof...(Rest))
        decltype(auto) operator[](std::integral_constant<size_t, Index>) const {
            return ::asd::detail::get<Index>(*this);
        }

        T head;
        plain_tuple<Rest...> tail;
    };

    template <class T>
    struct plain_tuple<T>
    {
        constexpr plain_tuple(const T & head) :
            head(head) {}
        constexpr plain_tuple(T && head) requires (!is_const_rvalue_v<T>) :
            head(std::move(head)) {}

        template <size_t Index> requires(Index == 0)
        decltype(auto) operator[](std::integral_constant<size_t, Index>) {
            return head;
        }

        template <size_t Index> requires(Index == 0)
        decltype(auto) operator[](std::integral_constant<size_t, Index>) const {
            return head;
        }

        T head;
    };

    template <>
    struct plain_tuple<> {};

    template <size_t Index, class ... Args>
    constexpr decltype(auto) get(const plain_tuple<Args...> & t) noexcept {
        return ::asd::detail::get<Index>(t);
    }

    template <size_t Index, class ... Args>
    constexpr decltype(auto) get(plain_tuple<Args...> & t) noexcept {
        return ::asd::detail::get<Index>(t);
    }

    template <size_t Index, class ... Args>
    constexpr decltype(auto) get(plain_tuple<Args...> && t) noexcept {
        return ::asd::detail::get<Index>(std::move(t));
    }

    template <class T, class ... Args>
    constexpr decltype(auto) get(const plain_tuple<Args...> & t) noexcept {
        constexpr auto Index = meta::find_v<meta::types<Args...>, T>;
        static_assert(Index >= 0, "Tuple doesn't contain any element with type T");

        return ::asd::detail::get<Index>(t);
    }

    template <class T, class ... Args>
    constexpr decltype(auto) get(plain_tuple<Args...> & t) noexcept {
        constexpr auto Index = meta::find_v<meta::types<Args...>, T>;
        static_assert(Index >= 0, "Tuple doesn't contain any element with type T");

        return ::asd::detail::get<Index>(t);
    }

    template <class T, class ... Args>
    constexpr decltype(auto) get(plain_tuple<Args...> && t) noexcept {
        constexpr auto Index = meta::find_v<meta::types<Args...>, T>;
        static_assert(Index >= 0, "Tuple doesn't contain any element with type T");

        return ::asd::detail::get<Index>(std::move(t));
    }

    template <class ... Args>
    [[nodiscard]] constexpr plain_tuple<plain<Args>...> make_plain_tuple(Args && ... args) {
        return plain_tuple<plain<Args>...>(std::forward<Args>(args)...);
    }

    template <class ... Args>
    [[nodiscard]] constexpr plain_tuple<Args &&...> forward_as_plain_tuple(Args && ... args) {
        return plain_tuple<Args &&...>(std::forward<Args>(args)...);
    }

    template <class T>
    struct is_plain_tuple : std::false_type {};

    template <class ... T>
    struct is_plain_tuple<plain_tuple<T...>> : std::true_type {};

    template <class T>
    constexpr bool is_plain_tuple_v = is_plain_tuple<T>::value;
}

namespace std
{
    template <size_t Idx, typename ... T>
    struct tuple_element<Idx, asd::plain_tuple<T...>> : tuple_element<Idx, asd::meta::types<T...>> {};

    template <typename ... T>
    struct tuple_size<asd::plain_tuple<T...>> : integral_constant<size_t, sizeof ... (T)> {};
}

static_assert(std::is_standard_layout_v<asd::plain_tuple<std::array<float, 3>, std::array<int, 2>, std::array<char, 4>>>);
static_assert(asd::meta::tuple_like<asd::plain_tuple<std::array<float, 3>, std::array<int, 2>, std::array<char, 4>>>);
