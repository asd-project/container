//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <utility>
#include <string>
#include <string_view>

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd
{
    constexpr auto hash_offset =
        std::conditional_t<(sizeof(size_t) < 8),
            std::integral_constant<uint32_t, 0x811C9DC5>,
            std::integral_constant<uint64_t, 0xCBF29CE484222325>
        >::value;

    constexpr auto hash_prime =
        std::conditional_t<(sizeof(size_t) < 8),
            std::integral_constant<uint32_t, 0x1000193>,
            std::integral_constant<uint64_t, 0x100000001B3>
        >::value;

    namespace detail
    {
        template <size_t I, meta::tuple_like T>
        constexpr size_t hash_combine(const T & v, const size_t value) noexcept {
            if constexpr (I >= std::tuple_size_v<T>) {
                return value;
            } else {
                auto & element = get<I>(v);
                return hash_combine<I + 1>(v, (value ^ std::hash<plaintype(element)>{}(element)) * hash_prime);
            }
        }
    }

    template <meta::tuple_like T>
    constexpr size_t hash_combine(const T & v, const size_t value = hash_offset) noexcept {
        return ::asd::detail::hash_combine<0>(v, value);
    }

    constexpr size_t hash_string(const char * str, const size_t value = hash_offset) noexcept {
        return *str ? hash_string(str + 1, (value ^ static_cast<size_t>(*str)) * hash_prime) : value;
    }

    constexpr bool str_less(const char * lhs, const char * rhs) noexcept {
        return *lhs && *rhs && *lhs == *rhs ? str_less(lhs + 1, rhs + 1) : *lhs < *rhs;
    }

    constexpr bool str_equal(const char * lhs, const char * rhs) noexcept {
        return *lhs == *rhs && (*lhs == '\0' || str_equal(lhs + 1, rhs + 1));
    }

    template <class T>
    struct hash : std::hash<T> {};

    template <class Ch, class Traits>
    struct hash<std::basic_string_view<Ch, Traits>>
    {
        constexpr size_t operator()(const std::basic_string_view<Ch, Traits> & str) const {
            return hash_string(str.data());
        }
    };

    template <>
    struct hash<const char *>
    {
        constexpr size_t operator()(const char * str) const {
            return hash_string(str);
        }
    };

    // support heterogeneous string lookup
    template <class Ch, class Traits, class Alloc>
    struct hash<std::basic_string<Ch, Traits, Alloc>> :
        std::hash<const char *>,
        std::hash<std::basic_string<Ch, Traits, Alloc>>,
        std::hash<std::basic_string_view<Ch, Traits>>
    {
        using std::hash<const char *>::operator();
        using std::hash<std::basic_string<Ch, Traits, Alloc>>::operator();
        using std::hash<std::basic_string_view<Ch, Traits>>::operator();
    };
}
