//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <utility>
#include <stdexcept>
#include <array>
#include <vector>

#include <boost/assert.hpp>
#include <boost/throw_exception.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    template <class T>
    class span
    {
    public:
        using iterator = T *;
        using reverse_iterator = T *;
        using const_iterator = const T *;
        using const_reverse_iterator = const T *;

        constexpr span() noexcept : _begin(nullptr), _end(nullptr) {}
        constexpr span(std::nullptr_t) noexcept : _begin(nullptr), _end(nullptr) {}
        constexpr span(const span & s) noexcept : _begin(s._begin), _end(s._end) {}
        explicit constexpr span(T * ptr, size_t size) noexcept : _begin(ptr), _end(ptr + size) {}
        explicit constexpr span(T * begin, T * end) noexcept : _begin(begin), _end(end) {}

        template <size_t N>
        constexpr span(T (&a)[N]) noexcept : _begin(a), _end(a + N) {}
        template <size_t N>
        constexpr span(T (&&a)[N]) noexcept : _begin(a), _end(a + N) {}

        template <size_t N>
        constexpr span(std::array<T, N> & a) noexcept : _begin(a.data()), _end(a.data() + a.size()) {}
        template <size_t N>
        constexpr span(std::array<T, N> && a) noexcept : _begin(a.data()), _end(a.data() + a.size()) {}

        template <class Allocator>
        span(std::vector<T, Allocator> & v) noexcept : _begin(v.data()), _end(v.data() + v.size()) {}
        template <class Allocator>
        span(std::vector<T, Allocator> && v) noexcept : _begin(v.data()), _end(v.data() + v.size()) {}

        constexpr span & operator = (const span & s) noexcept {
            _begin = s._begin;
            _end = s._end;

            return *this;
        }

        constexpr bool operator == (const span & s) const noexcept {
            return std::equal(begin(), end(), s.begin(), s.end());
        }

        constexpr bool operator != (const span & s) const noexcept {
            return !operator == (s);
        }

        constexpr void assign(T * begin, T * end) noexcept {
            this->_begin = begin;
            this->_end = end;
        }

        constexpr T & at(size_t index) {
            BOOST_ASSERT_MSG(_begin, "Data is not initialized");

            if (_begin + index >= _end) {
                BOOST_THROW_EXCEPTION(std::out_of_range("Index is out of bounds"));
            }

            return _begin[index];
        }

        constexpr const T & at(size_t index) const {
            BOOST_ASSERT_MSG(_begin, "Data is not initialized");

            if (_begin + index >= _end) {
                BOOST_THROW_EXCEPTION(std::out_of_range("Index is out of bounds"));
            }

            return _begin[index];
        }

        constexpr T & operator[](size_t index) noexcept {
            return _begin[index];
        }

        constexpr const T & operator[](size_t index) const noexcept {
            return _begin[index];
        }

        constexpr iterator begin() const noexcept {
            return _begin;
        }

        constexpr iterator end() const noexcept {
            return _end;
        }

        constexpr reverse_iterator rbegin() const noexcept {
            return _end;
        }

        constexpr reverse_iterator rend() const noexcept {
            return _begin;
        }

        constexpr const_iterator cbegin() const noexcept {
            return _begin;
        }

        constexpr const_iterator cend() const noexcept {
            return _end;
        }

        constexpr const_reverse_iterator crbegin() const noexcept {
            return _end;
        }

        constexpr const_reverse_iterator crend() const noexcept {
            return _begin;
        }

        constexpr bool empty() const noexcept {
            return _begin == _end;
        }

        constexpr T * data() noexcept {
            return _begin;
        }

        constexpr const T * data() const noexcept{
            return _begin;
        }

        constexpr size_t size() const noexcept {
            return _end - _begin;
        }

        constexpr void reset() noexcept {
            _begin = _end = nullptr;
        }

        constexpr auto release() noexcept {
            _end = nullptr;
            return std::exchange(_begin, nullptr);
        }

    private:
        T * _begin;
        T * _end;
    };

    template <class T>
    class span<const T>
    {
    public:
        using iterator = const T *;
        using reverse_iterator = const T *;
        using const_iterator = const T *;
        using const_reverse_iterator = const T *;

        constexpr span() noexcept : _begin(nullptr), _end(nullptr) {}
        constexpr span(std::nullptr_t) noexcept : _begin(nullptr), _end(nullptr) {}
        constexpr span(const span & s) noexcept : _begin(s._begin), _end(s._end) {}
        constexpr span(const span<T> & s) noexcept : _begin(s._begin), _end(s._end) {}
        explicit constexpr span(const T * ptr, size_t size) noexcept : _begin(ptr), _end(ptr + size) {}
        explicit constexpr span(const T * begin, const T * end) noexcept : _begin(begin), _end(end) {}

        template <size_t N>
        constexpr span(const T (&a)[N]) noexcept : _begin(a), _end(a + N) {}

        template <size_t N>
        constexpr span(const std::array<T, N> & a) noexcept : _begin(a.data()), _end(a.data() + a.size()) {}

        template <class Allocator>
        span(const std::vector<T, Allocator> & v) noexcept : _begin(v.data()), _end(v.data() + v.size()) {}

        constexpr span & operator = (const span & s) noexcept {
            _begin = s._begin;
            _end = s._end;

            return *this;
        }

        constexpr bool operator == (const span & s) const noexcept {
            return std::equal(begin(), end(), s.begin(), s.end());
        }

        constexpr bool operator == (const span<T> & s) const noexcept {
            return std::equal(begin(), end(), s.begin(), s.end());
        }

        constexpr bool operator != (const span & s) const noexcept {
            return !operator == (s);
        }

        constexpr bool operator != (const span<T> & s) const noexcept {
            return !operator == (s);
        }

        constexpr const T & at(size_t index) const {
            BOOST_ASSERT_MSG(_begin, "Data is not initialized");

            if (_begin + index >= _end) {
                BOOST_THROW_EXCEPTION(std::out_of_range("Index is out of bounds"));
            }

            return _begin[index];
        }

        constexpr const T & operator[](size_t index) const noexcept {
            return _begin[index];
        }

        constexpr const_iterator begin() const noexcept {
            return _begin;
        }

        constexpr const_iterator end() const noexcept {
            return _end;
        }

        constexpr const_reverse_iterator rbegin() const noexcept {
            return _end;
        }

        constexpr const_reverse_iterator rend() const noexcept {
            return _begin;
        }

        constexpr const_iterator cbegin() const noexcept {
            return _begin;
        }

        constexpr const_iterator cend() const noexcept {
            return _end;
        }

        constexpr const_reverse_iterator crbegin() const noexcept {
            return _end;
        }

        constexpr const_reverse_iterator crend() const noexcept {
            return _begin;
        }

        constexpr bool empty() const noexcept {
            return _begin == _end;
        }

        constexpr const T * data() const noexcept {
            return _begin;
        }

        constexpr size_t size() const noexcept {
            return _end - _begin;
        }

        constexpr void reset() noexcept {
            _begin = _end = nullptr;
        }

        constexpr auto release() noexcept {
            _end = nullptr;
            return std::exchange(_begin, nullptr);
        }

    private:
        const T * _begin;
        const T * _end;
    };

    template <>
    class span<void>
    {
    public:
        constexpr span() noexcept : _begin(nullptr), _end(nullptr) {}
        constexpr span(std::nullptr_t) noexcept : _begin(nullptr), _end(nullptr) {}
        constexpr span(const span & s) noexcept : _begin(s._begin), _end(s._end) {}
        constexpr span(span && s) noexcept : _begin(std::exchange(s._begin, nullptr)), _end(std::exchange(s._end, nullptr)) {}

        template <class T> requires (std::is_same_v<T, void>)
        explicit constexpr span(T * ptr, size_t size) noexcept : _begin(ptr), _end(static_cast<std::byte *>(ptr) + size) {}
        template <class T> requires (!std::is_same_v<T, void>)
        explicit constexpr span(T * ptr, size_t size) noexcept : _begin(ptr), _end(ptr + size) {}

        explicit constexpr span(void * begin, void * end) noexcept : _begin(begin), _end(end) {}

        template <class T, size_t N>
        constexpr span(T (&a)[N]) noexcept : _begin(a), _end(a + N) {}
        template <class T, size_t N>
        constexpr span(T (&&a)[N]) noexcept : _begin(a), _end(a + N) {}

        template <class T, size_t N>
        constexpr span(std::array<T, N> & a) noexcept : _begin(a.data()), _end(a.data() + a.size()) {}
        template <class T, size_t N>
        constexpr span(std::array<T, N> && a) noexcept : _begin(a.data()), _end(a.data() + a.size()) {}

        template <class T, class Allocator>
        span(std::vector<T, Allocator> & v) noexcept : _begin(v.data()), _end(v.data() + v.size()) {}
        template <class T, class Allocator>
        span(std::vector<T, Allocator> && v) noexcept : _begin(v.data()), _end(v.data() + v.size()) {}

        template <class T> requires (!std::is_const_v<T>)
        explicit constexpr span(const span<T> & v) noexcept : _begin(v.begin()), _end(v.end()) {}

        constexpr span & operator = (const span & s) noexcept {
            _begin = s._begin;
            _end = s._end;

            return *this;
        }

        template <class T>
        constexpr bool operator == (const span<T> & s) const noexcept {
            return _begin == s._begin && _end == s._end;
        }

        template <class T>
        constexpr bool operator != (const span<T> & s) const noexcept {
            return _begin != s._begin || _end != s._end;
        }

        constexpr void assign(void *&& begin, void *&& end) noexcept {
            std::swap(this->_begin, begin);
            std::swap(this->_end, end);
        }

        constexpr bool empty() const noexcept {
            return _begin == _end;
        }

        constexpr void * data() noexcept {
            return _begin;
        }

        constexpr const void * data() const noexcept {
            return _begin;
        }

        constexpr size_t size() const noexcept {
            return static_cast<const std::byte *>(_end) - static_cast<const std::byte *>(_begin);
        }

        constexpr void reset() noexcept {
            _begin = _end = nullptr;
        }

        constexpr auto release() noexcept {
            _end = nullptr;
            return std::exchange(_begin, nullptr);
        }

    private:
        void * _begin;
        void * _end;
    };

    template <>
    class span<const void>
    {
    public:
        constexpr span() noexcept : _begin(nullptr), _end(nullptr) {}
        constexpr span(std::nullptr_t) noexcept : _begin(nullptr), _end(nullptr) {}
        constexpr span(const span & s) noexcept : _begin(s._begin), _end(s._end) {}

        template <class T> requires (std::is_same_v<T, void>)
        explicit constexpr span(const T * ptr, size_t size) noexcept : _begin(ptr), _end(static_cast<const std::byte *>(ptr) + size) {}

        template <class T> requires (!std::is_same_v<T, void>)
        explicit constexpr span(const T * ptr, size_t size) noexcept : _begin(ptr), _end(ptr + size) {}

        explicit constexpr span(const void * begin, const void * end) noexcept : _begin(begin), _end(end) {}

        template <class T, size_t N>
        constexpr span(const T (&a)[N]) noexcept : _begin(a), _end(a + N) {}

        template <class T, size_t N>
        constexpr span(const std::array<T, N> & a) noexcept : _begin(a.data()), _end(a.data() + a.size()) {}

        template <class T, class Allocator>
        span(const std::vector<T, Allocator> & v) noexcept : _begin(v.data()), _end(v.data() + v.size()) {}

        template <class T>
        explicit constexpr span(const span<T> & v) noexcept : _begin(v.begin()), _end(v.end()) {}

        constexpr span & operator = (const span & s) noexcept {
            _begin = s._begin;
            _end = s._end;

            return *this;
        }

        template <class T>
        constexpr bool operator == (const span<T> & s) const noexcept {
            return _begin == s._begin;
        }

        template <class T>
        constexpr bool operator != (const span<T> & s) const noexcept {
            return _begin != s._begin;
        }

        constexpr bool empty() const noexcept {
            return _begin == _end;
        }

        constexpr const void * data() const noexcept {
            return _begin;
        }

        constexpr size_t size() const noexcept {
            return static_cast<const std::byte *>(_end) - static_cast<const std::byte *>(_begin);
        }

        constexpr void reset() noexcept {
            _begin = _end = nullptr;
        }

        constexpr auto release() noexcept {
            _end = nullptr;
            return std::exchange(_begin, nullptr);
        }

    private:
        const void * _begin;
        const void * _end;
    };

    span(nullptr_t) -> span<void>;

    template <class T>
    span(T * begin, T * end) -> span<T>;

    template <class T>
    span(T * begin, size_t size) -> span<T>;

    template <class T, class Allocator>
    span(const std::vector<T, Allocator> & v) -> span<const T>;

    template <class T, class Allocator>
    span(std::vector<T, Allocator> & v) -> span<T>;

    template <class T, class Allocator>
    span(std::vector<T, Allocator> && v) -> span<T>;

    template <class T, size_t N>
    span(const std::array<T, N> & v) -> span<const T>;

    template <class T, size_t N>
    span(std::array<T, N> & v) -> span<T>;

    template <class T, size_t N>
    span(std::array<T, N> && v) -> span<T>;
}
