//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <array>
#include <vector>

//---------------------------------------------------------------------------

namespace asd
{
    template <class _Ty, class _Alloc = std::allocator<_Ty>>
    using array_list = std::vector<_Ty, _Alloc>;

    template <typename T, typename U> requires(std::is_convertible_v<U, T>)
    array_list<T> & operator << (array_list<T> & a, U && value) {
        a.emplace_back(std::forward<U>(value));
        return a;
    }
}
