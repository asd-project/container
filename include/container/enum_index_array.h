//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <array>
#include <compare>

#include <magic_enum.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    template <class E, class V>
    class enum_index_iterator
    {
    public:
        using value_type = std::pair<E, V &>;

        constexpr enum_index_iterator(V * base, size_t index) :
            _base(base), _index(index) {}

        constexpr enum_index_iterator(const enum_index_iterator &) noexcept = default;
        constexpr enum_index_iterator & operator = (const enum_index_iterator &) noexcept = default;

        constexpr enum_index_iterator & operator ++ () {
            ++_index;
            return *this;
        }

        constexpr enum_index_iterator operator ++ (int) {
            enum_index_iterator output(*this);
            ++_index;

            return output;
        }

        constexpr value_type operator * () const {
            return {static_cast<E>(_index), *(_base + _index)};
        }

        friend constexpr std::strong_ordering operator <=> (enum_index_iterator, enum_index_iterator) noexcept = default;

    private:
        V * _base;
        size_t _index;
    };

    template <class E, class V>
    class enum_index_array : public std::array<V, magic_enum::enum_count<E>()>
    {
        using base = std::array<V, magic_enum::enum_count<E>()>;

    public:
        using iterator = enum_index_iterator<E, V>;
        using const_iterator = enum_index_iterator<E, const V>;

        constexpr auto begin() {
            return iterator{this->data(), 0};
        }

        constexpr auto begin() const {
            return const_iterator{this->data(), 0};
        }

        constexpr auto end() {
            return iterator{this->data(), this->size()};
        }

        constexpr auto end() const {
            return const_iterator{this->data(), this->size()};
        }

        constexpr V & operator [] (E index) noexcept {
            return base::operator[](static_cast<size_t>(index));
        }

        constexpr const V & operator [] (E index) const noexcept {
            return base::operator[](static_cast<size_t>(index));
        }
    };
}
