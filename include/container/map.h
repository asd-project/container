//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <map>
#include <unordered_map>
#include <tsl/hopscotch_map.h>
#include <string>
#include <string_view>

#include <meta/macro.h>
#include <container/container.h>
#include <container/hash.h>

//---------------------------------------------------------------------------

namespace asd
{
    template <class K, class T, class Pred = std::less<>, class Alloc = std::allocator<std::pair<const K, T>>>
    using dictionary = std::map<K, T, Pred, Alloc>;

    template <class K, class T, class Pred = std::less<>, class Alloc = std::allocator<std::pair<const K, T>>>
    using multidictionary = std::multimap<K, T, Pred, Alloc>;

    template <class K, class T, class Pred = std::less<>, class Alloc = std::allocator<std::pair<const K, T>>>
    using ordered_map = std::map<K, T, Pred, Alloc>;

    template <class K, class T, class Pred = std::less<>, class Alloc = std::allocator<std::pair<const K, T>>>
    using ordered_multimap = std::multimap<K, T, Pred, Alloc>;

    #ifdef ASD_USE_STD_UNORDERED_MAP
        template <class K, class T, class Hash = hash<K>, class EqualTo = std::equal_to<>, class Alloc = std::allocator<std::pair<const K, T>>>
        using map = std::unordered_map<K, T, Hash, EqualTo, Alloc>;
    #else
        template <class K, class T, class Hash = hash<K>, class EqualTo = std::equal_to<>, class Alloc = std::allocator<std::pair<K, T>>>
        using map = tsl::hopscotch_map<K, T, Hash, EqualTo, Alloc, 30, true>;
    #endif

    template <class K, class T, class Hash = hash<K>, class EqualTo = std::equal_to<>, class Alloc = std::allocator<std::pair<const K, T>>>
    using stable_map = std::unordered_map<K, T, Hash, EqualTo, Alloc>;

    template <class K, class T, class Hasher =  hash<K>, class Eq = std::equal_to<>, class Alloc = std::allocator<std::pair<const K, T>>>
    using multimap = std::unordered_multimap<K, T, Hasher, Eq, Alloc>;

    template <class K, class T, class Pred = std::less<>, class Alloc = std::allocator<std::pair<const K, T>>>
    using dict = dictionary<K, T, Pred, Alloc>;

    template <class K, class T, class Pred = std::less<>, class Alloc = std::allocator<std::pair<const K, T>>>
    using multidict = multidictionary<K, T, Pred, Alloc>;

    template <class It>
    auto && key_of(It && it) {
        return std::forward<It>(it)->first;
    }

    template <class It>
    auto && value_of(It && it) {
        return std::forward<It>(it)->second;
    }

    namespace pmr
    {
        template <class K, class T, class Pred = std::less<>>
        using dictionary = std::map<K, T, Pred, std::pmr::polymorphic_allocator<std::pair<const K, T>>>;

        template <class K, class T, class Pred = std::less<>>
        using multidictionary = std::multimap<K, T, Pred, std::pmr::polymorphic_allocator<std::pair<const K, T>>>;

        template <class K, class T, class Pred = std::less<>>
        using ordered_map = std::map<K, T, Pred, std::pmr::polymorphic_allocator<std::pair<const K, T>>>;

        template <class K, class T, class Pred = std::less<>>
        using ordered_multimap = std::multimap<K, T, Pred, std::pmr::polymorphic_allocator<std::pair<const K, T>>>;

        #ifdef ASD_USE_STD_UNORDERED_MAP
            template <class K, class T, class Hash = hash<K>, class EqualTo = std::equal_to<>>
            using map = std::unordered_map<K, T, Hash, EqualTo, std::pmr::polymorphic_allocator<std::pair<const K, T>>>;
        #else
            template <class K, class T, class Hash = hash<K>, class EqualTo = std::equal_to<>>
            using map = tsl::hopscotch_map<K, T, Hash, EqualTo, std::pmr::polymorphic_allocator<std::pair<K, T>>, 30, true>;
        #endif

        template <class K, class T, class Hash = hash<K>, class EqualTo = std::equal_to<>>
        using stable_map = std::unordered_map<K, T, Hash, EqualTo, std::pmr::polymorphic_allocator<std::pair<const K, T>>>;

        template <class K, class T, class Hasher =  hash<K>, class Eq = std::equal_to<>>
        using multimap = std::unordered_multimap<K, T, Hasher, Eq, std::pmr::polymorphic_allocator<std::pair<const K, T>>>;

        template <class K, class T, class Pred = std::less<>>
        using dict = dictionary<K, T, Pred>;

        template <class K, class T, class Pred = std::less<>>
        using multidict = multidictionary<K, T, Pred>;
    }
}
