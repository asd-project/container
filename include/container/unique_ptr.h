//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <memory>
#include <memory_resource>

//---------------------------------------------------------------------------

namespace asd
{
    using std::unique_ptr;

    namespace pmr
    {
        struct size_info
        {
            size_t size;
            size_t alignment;
        };

        template <class T>
        inline const pmr::size_info type_size_info {sizeof(T), alignof(T)};

        class polymorphic_deleter
        {
        public:
            template <typename T>
            polymorphic_deleter(const std::pmr::polymorphic_allocator<T> & allocator) :
                _resource(allocator.resource()),
                _info(&type_size_info<T>)
            {}

            template <typename T>
            void operator () (T * ptr) {
                std::destroy_at(ptr);
                _resource->deallocate(ptr, _info->size, _info->alignment);
            }

        private:
            std::pmr::memory_resource * _resource;
            const pmr::size_info * _info;
        };

        template <class T>
        using unique_ptr = std::unique_ptr<T, polymorphic_deleter>;

        template <class T, class ... A>
        auto make_unique(std::pmr::polymorphic_allocator<T> allocator, A && ... args) {
            return pmr::unique_ptr<T>{allocator.new_object<T>(std::forward<A>(args)...), allocator};
        }
    }
}
