//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <container/container.h>
#include <set>
#include <unordered_set>

#include <tsl/hopscotch_set.h>
#include <container/hash.h>

//---------------------------------------------------------------------------

namespace asd
{
    template <class T, class Pred = std::less<T>, class Alloc = std::allocator<T>>
    using ordered_set = std::set<T, Pred, Alloc>;

    template <class T, class Hasher = hash<T>, class Eq = std::equal_to<T>, class Alloc = std::allocator<T>>
    using set = tsl::hopscotch_set<T, Hasher, Eq, Alloc>;
}
