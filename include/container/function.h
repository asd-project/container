/**
 * ----------------------------------------------------------------------------
 *
 *   Created by Malte Skarupke
 *   Modified by Ilya Pribylsky
 *
 *   Changes:
 *       Added move_only_function
 *       Renamed namespace from `func` to `asd`
 *       Improved exceptions handling
 *       Changed/removed macro-definitions
 *       Updated code formatting
 *
 *   Original file is here: https://github.com/skarupke/std_function
 *
 * ----------------------------------------------------------------------------
 */

#pragma once

#include <utility>
#include <type_traits>
#include <functional>
#include <exception>
#include <typeinfo>
#include <memory>
#include <tuple>

#include <ctti/nameof.hpp>

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif

namespace asd
{
    template <class F>
    struct bad_function_call : std::exception
    {
        const char * what() const noexcept override {
            static const std::string s = "Bad function call for " + ctti::nameof<F>();
            return s.c_str();
        }
    };

    template <typename>
    struct force_function_heap_allocation : std::false_type {};

    template <typename>
    class function;

    template <typename>
    class move_only_function;

    namespace detail
    {
        struct function_manager;
        struct move_only_function_manager;

        struct functor_padding
        {
        protected:
            size_t padding_first;
            size_t padding_second;
        };

        struct empty_struct
        {
        };

        template <typename Result, typename... Arguments>
        Result empty_call(const functor_padding &, Arguments...) {
            BOOST_THROW_EXCEPTION(bad_function_call<Result(Arguments...)>());
        }

        template <typename T, typename Allocator>
        constexpr bool is_inplace_allocated_v =
            // so that it fits
            sizeof(T) <= sizeof(functor_padding) &&
            // so that it will be aligned
            std::alignment_of<functor_padding>::value % std::alignment_of<T>::value == 0 &&
            // so that we can offer noexcept move
            std::is_nothrow_move_constructible<T>::value &&
            // so that the user can override it
            !force_function_heap_allocation<T>::value;

        template <typename T>
        T to_functor(T && func) {
            return std::forward<T>(func);
        }

        template <typename Result, typename Class, typename... Arguments>
        auto to_functor(Result (Class::*func)(Arguments...)) -> decltype(std::mem_fn(func)) {
            return std::mem_fn(func);
        }

        template <typename Result, typename Class, typename... Arguments>
        auto to_functor(Result (Class::*func)(Arguments...) const) -> decltype(std::mem_fn(func)) {
            return std::mem_fn(func);
        }

        template <typename T>
        struct functor_type
        {
            typedef decltype(to_functor(std::declval<T>())) type;
        };

        template <typename T>
            requires (not std::equality_comparable_with<T, std::nullptr_t>)
        bool is_null(const T &) {
            return false;
        }

        template <typename T>
            requires std::equality_comparable_with<T, std::nullptr_t>
        bool is_null(const T & value) {
            return value == nullptr;
        }

        template <typename, typename>
        struct is_valid_function_argument
        {
            static const bool value = false;
        };

        template <typename Result, typename... Arguments>
        struct is_valid_function_argument<function<Result(Arguments...)>, Result(Arguments...)>
        {
            static const bool value = false;
        };

        template <typename Result, typename... Arguments>
        struct is_valid_function_argument<move_only_function<Result(Arguments...)>, Result(Arguments...)>
        {
            static const bool value = false;
        };

        template <typename T, typename Result, typename... Arguments>
        struct is_valid_function_argument<T, Result(Arguments...)>
        {
            template <typename U>
            static decltype(to_functor(std::declval<U>())(std::declval<Arguments>()...)) check(U *);
            template <typename>
            static empty_struct check(...);

            static const bool value = std::is_convertible<decltype(check<T>(nullptr)), Result>::value;
        };

        template <class Manager>
        struct manager_storage_base
        {
            template <typename Allocator>
            Allocator & get_allocator() noexcept {
                return reinterpret_cast<Allocator &>(manager);
            }

            template <typename Allocator>
            const Allocator & get_allocator() const noexcept {
                return reinterpret_cast<const Allocator &>(manager);
            }

            functor_padding functor;
            const Manager * manager;
        };

        using manager_storage_type = manager_storage_base<function_manager>;
        using move_only_manager_storage_type = manager_storage_base<move_only_function_manager>;

        template <typename T, typename Allocator, typename Enable = void>
        struct function_manager_inplace_specialization
        {
            template <typename Result, typename... Arguments>
            static Result call(const functor_padding & storage, Arguments... arguments) {
                // do not call get_functor_ref because I want this function to be fast
                // in debug when nothing gets inlined
                return const_cast<T &>(reinterpret_cast<const T &>(storage))(std::forward<Arguments>(arguments)...);
            }

            template <class Manager>
            static void store_functor(manager_storage_base<Manager> & storage, T to_store) {
                new(&get_functor_ref(storage)) T(std::forward<T>(to_store));
            }

            template <class Manager>
            static void move_functor(manager_storage_base<Manager> & lhs, manager_storage_base<Manager> && rhs) noexcept {
                new(&get_functor_ref(lhs)) T(std::move(get_functor_ref(rhs)));
            }

            template <class Manager>
            static void destroy_functor(Allocator &, manager_storage_base<Manager> & storage) noexcept {
                get_functor_ref(storage).~T();
            }

            template <class Manager>
            static T & get_functor_ref(const manager_storage_base<Manager> & storage) noexcept {
                return const_cast<T &>(reinterpret_cast<const T &>(storage.functor));
            }
        };

        template <typename T, typename Allocator>
        struct function_manager_inplace_specialization<T, Allocator, typename std::enable_if<!is_inplace_allocated_v<T, Allocator>>::type>
        {
            template <typename Result, typename... Arguments>
            static Result call(const functor_padding & storage, Arguments... arguments) {
                // do not call get_functor_ptr_ref because I want this function to be fast
                // in debug when nothing gets inlined
                return (*reinterpret_cast<const typename std::allocator_traits<Allocator>::pointer &>(storage))(std::forward<Arguments>(arguments)...);
            }

            template <class Manager>
            static void store_functor(manager_storage_base<Manager> & self, T to_store) {
                Allocator & allocator = self.get_allocator<Allocator>();;
                static_assert(sizeof(typename std::allocator_traits<Allocator>::pointer) <= sizeof(self.functor), "The allocator's pointer type is too big");
                typename std::allocator_traits<Allocator>::pointer * ptr = new(&get_functor_ptr_ref(self)) typename std::allocator_traits<Allocator>::pointer(std::allocator_traits<Allocator>::allocate(allocator, 1));
                std::allocator_traits<Allocator>::construct(allocator, *ptr, std::forward<T>(to_store));
            }

            template <class Manager>
            static void move_functor(manager_storage_base<Manager> & lhs, manager_storage_base<Manager> && rhs) noexcept {
                static_assert(std::is_nothrow_move_constructible<typename std::allocator_traits<Allocator>::pointer>::value, "we can't offer a noexcept swap if the pointer type is not nothrow move constructible");
                new(&get_functor_ptr_ref(lhs)) typename std::allocator_traits<Allocator>::pointer(std::move(get_functor_ptr_ref(rhs)));
                // this next assignment makes the destroy function easier
                get_functor_ptr_ref(rhs) = nullptr;
            }

            template <class Manager>
            static void destroy_functor(Allocator & allocator, manager_storage_base<Manager> & storage) noexcept {
                typename std::allocator_traits<Allocator>::pointer & pointer = get_functor_ptr_ref(storage);
                if(!pointer) {
                    return;
                }
                std::allocator_traits<Allocator>::destroy(allocator, pointer);
                std::allocator_traits<Allocator>::deallocate(allocator, pointer, 1);
            }

            template <class Manager>
            static T & get_functor_ref(const manager_storage_base<Manager> & storage) noexcept {
                return *get_functor_ptr_ref(storage);
            }

            template <class Manager>
            static typename std::allocator_traits<Allocator>::pointer & get_functor_ptr_ref(manager_storage_base<Manager> & storage) noexcept {
                return reinterpret_cast<typename std::allocator_traits<Allocator>::pointer &>(storage.functor);
            }

            template <class Manager>
            static const typename std::allocator_traits<Allocator>::pointer & get_functor_ptr_ref(const manager_storage_base<Manager> & storage) noexcept {
                return reinterpret_cast<const typename std::allocator_traits<Allocator>::pointer &>(storage.functor);
            }
        };

        template <typename Manager, typename T, typename Allocator>
        static const Manager & get_default_manager();

        template <typename T, typename Allocator, typename Manager>
        static void setup_manager(manager_storage_base<Manager> & storage, Allocator && allocator) {
            new(&storage.get_allocator<Allocator>()) Allocator(std::move(allocator));
            storage.manager = &get_default_manager<Manager, T, Allocator>();
        }

        // this struct acts as a vtable. it is an optimization to prevent
        // code-bloat from rtti. see the documentation of boost::function
        struct function_manager
        {
            template <typename T, typename Allocator>
            inline static constexpr function_manager create_default_manager() {
#ifdef _MSC_VER
                function_manager result =
#else
                return function_manager
#endif
                    {
                        &templated_call_move_and_destroy < T, Allocator > ,
                        &templated_call_copy < T, Allocator > ,
                        &templated_call_copy_functor_only < T, Allocator > ,
                        &templated_call_destroy < T, Allocator > ,
#if ASD_OPTION_RTTI
                        &templated_call_type_id < T, Allocator > ,
                        &templated_call_target < T, Allocator >
#endif
                    };
#ifdef _MSC_VER
                return result;
#endif
            }

            void (* const call_move_and_destroy)(manager_storage_type & lhs, manager_storage_type && rhs);
            void (* const call_copy)(manager_storage_type & lhs, const manager_storage_type & rhs);
            void (* const call_copy_functor_only)(manager_storage_type & lhs, const manager_storage_type & rhs);
            void (* const call_destroy)(manager_storage_type & manager);
#if ASD_OPTION_RTTI
            const std::type_info & (* const call_type_id)();
            void * (* const call_target)(const manager_storage_type & manager, const std::type_info & type);
#endif

            template <typename T, typename Allocator>
            static void templated_call_move_and_destroy(manager_storage_type & lhs, manager_storage_type && rhs) {
                using specialization = function_manager_inplace_specialization<T, Allocator>;
                specialization::move_functor(lhs, std::move(rhs));
                specialization::destroy_functor(rhs.get_allocator<Allocator>(), rhs);
                setup_manager<T, Allocator>(lhs, std::move(rhs.get_allocator<Allocator>()));
                rhs.get_allocator<Allocator>().~Allocator();
            }

            template <typename T, typename Allocator>
            static void templated_call_copy(manager_storage_type & lhs, const manager_storage_type & rhs) {
                using specialization = function_manager_inplace_specialization<T, Allocator>;
                setup_manager<T, Allocator>(lhs, Allocator(rhs.get_allocator<Allocator>()));
                specialization::store_functor(lhs, specialization::get_functor_ref(rhs));
            }

            template <typename T, typename Allocator>
            static void templated_call_copy_functor_only(manager_storage_type & lhs, const manager_storage_type & rhs) {
                using specialization = function_manager_inplace_specialization<T, Allocator>;
                specialization::store_functor(lhs, specialization::get_functor_ref(rhs));
            }

            template <typename T, typename Allocator>
            static void templated_call_destroy(manager_storage_type & self) {
                using specialization = function_manager_inplace_specialization<T, Allocator>;
                specialization::destroy_functor(self.get_allocator<Allocator>(), self);
                self.get_allocator<Allocator>().~Allocator();
            }

#if ASD_OPTION_RTTI

            template <typename T, typename>
            static const std::type_info & templated_call_type_id() {
                return typeid(T);
            }

            template <typename T, typename Allocator>
            static void * templated_call_target(const manager_storage_type & self, const std::type_info & type) {
                using specialization = function_manager_inplace_specialization<T, Allocator>;
                if(type == typeid(T)) {
                    return &specialization::get_functor_ref(self);
                } else {
                    return nullptr;
                }
            }

#endif
        };

        // this struct acts as a vtable. it is an optimization to prevent
        // code-bloat from rtti. see the documentation of boost::function
        struct move_only_function_manager
        {
            template <typename T, typename Allocator>
            inline static constexpr move_only_function_manager create_default_manager() {
                return move_only_function_manager {
                    &templated_call_move_and_destroy < T, Allocator > ,
                    &templated_call_destroy < T, Allocator > ,
#if ASD_OPTION_RTTI
                    &templated_call_type_id < T, Allocator > ,
                    &templated_call_target < T, Allocator >
#endif
                };
            }

            void (* const call_move_and_destroy)(move_only_manager_storage_type & lhs, move_only_manager_storage_type && rhs);
            void (* const call_destroy)(move_only_manager_storage_type & manager);
#if ASD_OPTION_RTTI
            const std::type_info & (* const call_type_id)();
            void * (* const call_target)(const move_only_manager_storage_type & manager, const std::type_info & type);
#endif

            template <typename T, typename Allocator>
            static void templated_call_move_and_destroy(move_only_manager_storage_type & lhs, move_only_manager_storage_type && rhs) {
                using specialization = function_manager_inplace_specialization<T, Allocator>;
                specialization::move_functor(lhs, std::move(rhs));
                specialization::destroy_functor(rhs.get_allocator<Allocator>(), rhs);
                setup_manager<T, Allocator>(lhs, std::move(rhs.get_allocator<Allocator>()));
                rhs.get_allocator<Allocator>().~Allocator();
            }

            template <typename T, typename Allocator>
            static void templated_call_destroy(move_only_manager_storage_type & self) {
                using specialization = function_manager_inplace_specialization<T, Allocator>;
                specialization::destroy_functor(self.get_allocator<Allocator>(), self);
                self.get_allocator<Allocator>().~Allocator();
            }

#if ASD_OPTION_RTTI

            template <typename T, typename>
            static const std::type_info & templated_call_type_id() {
                return typeid(T);
            }

            template <typename T, typename Allocator>
            static void * templated_call_target(const move_only_manager_storage_type & self, const std::type_info & type) {
                using specialization = function_manager_inplace_specialization<T, Allocator>;
                if(type == typeid(T)) {
                    return &specialization::get_functor_ref(self);
                } else {
                    return nullptr;
                }
            }

#endif
        };

        template <typename Manager, typename T, typename Allocator>
        inline static const Manager & get_default_manager() {
            static constexpr auto default_manager = Manager::template create_default_manager<T, Allocator>();
            return default_manager;
        }

        template <typename Result, typename...Arguments>
        struct type_defer
        {
            using result_type = Result;
            using arguments_type = std::tuple<Arguments...>;
        };
    }

    template <typename Result, typename... Arguments>
    class function<Result(Arguments...)>
        : public asd::detail::type_defer<Result, Arguments...>
    {
    public:
        function() noexcept {
            initialize_empty();
        }

        function(std::nullptr_t) noexcept {
            initialize_empty();
        }

        function(function && other) noexcept {
            initialize_empty();
            swap(other);
        }

        function(const function & other)
            : call(other.call) {
            other.manager_storage.manager->call_copy(manager_storage, other.manager_storage);
        }

        template <typename T>
        function(T functor, typename std::enable_if<asd::detail::is_valid_function_argument<T, Result(Arguments...)>::value, asd::detail::empty_struct>::type = asd::detail::empty_struct())
            noexcept(asd::detail::is_inplace_allocated_v<T, std::allocator<typename asd::detail::functor_type<T>::type>>)
        {
            if (asd::detail::is_null(functor)) {
                initialize_empty();
            } else {
                typedef typename asd::detail::functor_type<T>::type functor_type;
                initialize(asd::detail::to_functor(std::forward<T>(functor)), std::allocator<functor_type>());
            }
        }

        template <typename Allocator>
        function(std::allocator_arg_t, const Allocator &) {
            // ignore the allocator because I don't allocate
            initialize_empty();
        }

        template <typename Allocator>
        function(std::allocator_arg_t, const Allocator &, std::nullptr_t) {
            // ignore the allocator because I don't allocate
            initialize_empty();
        }

        template <typename Allocator, typename T>
        function(std::allocator_arg_t, const Allocator & allocator, T functor, typename std::enable_if<asd::detail::is_valid_function_argument<T, Result(Arguments...)>::value, asd::detail::empty_struct>::type = asd::detail::empty_struct())
            noexcept(asd::detail::is_inplace_allocated_v<T, Allocator>)
        {
            if (asd::detail::is_null(functor)) {
                initialize_empty();
            } else {
                initialize(asd::detail::to_functor(std::forward<T>(functor)), Allocator(allocator));
            }
        }

        template <typename Allocator>
        function(std::allocator_arg_t, const Allocator & allocator, const function & other)
            : call(other.call) {
            typedef typename std::allocator_traits<Allocator>::template rebind_alloc<function> MyAllocator;

            // first try to see if the allocator matches the target type
            auto manager_for_allocator = &asd::detail::get_default_manager<typename std::allocator_traits<Allocator>::value_type, Allocator>();
            if(other.manager_storage.manager == manager_for_allocator) {
                asd::detail::setup_manager<typename std::allocator_traits<Allocator>::value_type, Allocator>(manager_storage, Allocator(allocator));
                manager_for_allocator->call_copy_functor_only(manager_storage, other.manager_storage);
            }
                // if it does not, try to see if the target contains my type. this
                // breaks the recursion of the last case. otherwise repeated copies
                // would allocate more and more memory
            else {
                auto manager_for_function = &asd::detail::get_default_manager<function, MyAllocator>();
                if(other.manager_storage.manager == manager_for_function) {
                    asd::detail::setup_manager<function, MyAllocator>(manager_storage, MyAllocator(allocator));
                    manager_for_function->call_copy_functor_only(manager_storage, other.manager_storage);
                } else {
                    // else store the other function as my target
                    initialize(other, MyAllocator(allocator));
                }
            }
        }

        template <typename Allocator>
        function(std::allocator_arg_t, const Allocator &, function && other) noexcept {
            // ignore the allocator because I don't allocate
            initialize_empty();
            swap(other);
        }

        function & operator =(function other) noexcept {
            swap(other);
            return *this;
        }

        ~function() noexcept {
            manager_storage.manager->call_destroy(manager_storage);
        }

        Result operator ()(Arguments... arguments) const {
            return call(manager_storage.functor, std::forward<Arguments>(arguments)...);
        }

        template <typename T, typename Allocator>
        void assign(T && functor, const Allocator & allocator) noexcept(asd::detail::is_inplace_allocated_v<T, Allocator>) {
            function(std::allocator_arg, allocator, functor).swap(*this);
        }

        void swap(function & other) noexcept {
            asd::detail::manager_storage_type temp_storage;
            other.manager_storage.manager->call_move_and_destroy(temp_storage, std::move(other.manager_storage));
            manager_storage.manager->call_move_and_destroy(other.manager_storage, std::move(manager_storage));
            temp_storage.manager->call_move_and_destroy(manager_storage, std::move(temp_storage));

            std::swap(call, other.call);
        }


#if ASD_OPTION_RTTI

        const std::type_info & target_type() const noexcept {
            return manager_storage.manager->call_type_id();
        }

        template <typename T>
        T * target() noexcept {
            return static_cast<T *>(manager_storage.manager->call_target(manager_storage, typeid(T)));
        }

        template <typename T>
        const T * target() const noexcept {
            return static_cast<const T *>(manager_storage.manager->call_target(manager_storage, typeid(T)));
        }

#endif

        operator bool() const noexcept {

#if !ASD_OPTION_EXCEPTIONS
            return call != nullptr;
#else
            return call != &asd::detail::empty_call<Result, Arguments...>;
#endif
        }

    private:
        asd::detail::manager_storage_type manager_storage;
        Result (* call)(const asd::detail::functor_padding &, Arguments...);

        template <typename T, typename Allocator>
        void initialize(T functor, Allocator && allocator) {
            call = &asd::detail::function_manager_inplace_specialization<T, Allocator>::template call<Result, Arguments...>;
            asd::detail::setup_manager<T, Allocator>(manager_storage, std::forward<Allocator>(allocator));
            asd::detail::function_manager_inplace_specialization<T, Allocator>::store_functor(manager_storage, std::forward<T>(functor));
        }

        typedef Result(* Empty_Function_Type)(Arguments...);

        void initialize_empty() noexcept {
            typedef std::allocator<Empty_Function_Type> Allocator;
            static_assert(asd::detail::is_inplace_allocated_v<Empty_Function_Type, Allocator>, "The empty function should benefit from small functor optimization");

            asd::detail::setup_manager<Empty_Function_Type, Allocator>(manager_storage, Allocator());
            asd::detail::function_manager_inplace_specialization<Empty_Function_Type, Allocator>::store_functor(manager_storage, nullptr);
#if !ASD_OPTION_EXCEPTIONS
            call = nullptr;
#else
            call = &asd::detail::empty_call<Result, Arguments...>;
#endif
        }
    };

    template <typename Result, typename... Arguments>
    class move_only_function<Result(Arguments...)>
        : public asd::detail::type_defer<Result, Arguments...>
    {
    public:
        move_only_function() noexcept {
            initialize_empty();
        }

        move_only_function(std::nullptr_t) noexcept {
            initialize_empty();
        }

        template <typename T>
        move_only_function(T functor, typename std::enable_if<asd::detail::is_valid_function_argument<T, Result(Arguments...)>::value, asd::detail::empty_struct>::type = asd::detail::empty_struct())
            noexcept(asd::detail::is_inplace_allocated_v<T, std::allocator<typename asd::detail::functor_type<T>::type>>)
        {
            if (asd::detail::is_null(functor)) {
                initialize_empty();
            } else {
                typedef typename asd::detail::functor_type<T>::type functor_type;
                initialize(asd::detail::to_functor(std::forward<T>(functor)), std::allocator<functor_type>());
            }
        }

        move_only_function(move_only_function && other) noexcept {
            initialize_empty();
            swap(other);
        }

        move_only_function(function<Result(Arguments...)> && other) noexcept {
            if (asd::detail::is_null(other)) {
                initialize_empty();
            } else {
                typedef typename asd::detail::functor_type<function<Result(Arguments...)>>::type functor_type;
                initialize(asd::detail::to_functor(std::move(other)), std::allocator<functor_type>());
            }
        }

        template <typename Allocator>
        move_only_function(std::allocator_arg_t, const Allocator &) {
            // ignore the allocator because I don't allocate
            initialize_empty();
        }

        template <typename Allocator>
        move_only_function(std::allocator_arg_t, const Allocator &, std::nullptr_t) {
            // ignore the allocator because I don't allocate
            initialize_empty();
        }

        template <typename Allocator, typename T>
        move_only_function(std::allocator_arg_t, const Allocator & allocator, T functor, typename std::enable_if<asd::detail::is_valid_function_argument<T, Result(Arguments...)>::value, asd::detail::empty_struct>::type = asd::detail::empty_struct())
            noexcept(asd::detail::is_inplace_allocated_v<T, Allocator>)
        {
            if (asd::detail::is_null(functor)) {
                initialize_empty();
            } else {
                initialize(asd::detail::to_functor(std::forward<T>(functor)), Allocator(allocator));
            }
        }

        template <typename Allocator>
        move_only_function(std::allocator_arg_t, const Allocator &, move_only_function && other) noexcept {
            // ignore the allocator because I don't allocate
            initialize_empty();
            swap(other);
        }

        template <typename Allocator>
        move_only_function(std::allocator_arg_t, const Allocator & allocator, function<Result(Arguments...)> && other) noexcept {
            if (asd::detail::is_null(other)) {
                initialize_empty();
            } else {
                initialize(asd::detail::to_functor(std::move(other)), Allocator(allocator));
            }
        }

        move_only_function & operator =(move_only_function other) noexcept {
            swap(other);
            return *this;
        }

        ~move_only_function() noexcept {
            manager_storage.manager->call_destroy(manager_storage);
        }

        Result operator ()(Arguments... arguments) const {
            return call(manager_storage.functor, std::forward<Arguments>(arguments)...);
        }

        template <typename T, typename Allocator>
        void assign(T && functor, const Allocator & allocator) noexcept(asd::detail::is_inplace_allocated_v<T, Allocator>) {
            move_only_function(std::allocator_arg, allocator, functor).swap(*this);
        }

        void swap(move_only_function & other) noexcept {
            asd::detail::move_only_manager_storage_type temp_storage;
            other.manager_storage.manager->call_move_and_destroy(temp_storage, std::move(other.manager_storage));
            manager_storage.manager->call_move_and_destroy(other.manager_storage, std::move(manager_storage));
            temp_storage.manager->call_move_and_destroy(manager_storage, std::move(temp_storage));

            std::swap(call, other.call);
        }

#if ASD_OPTION_RTTI

        const std::type_info & target_type() const noexcept {
            return manager_storage.manager->call_type_id();
        }

        template <typename T>
        T * target() noexcept {
            return static_cast<T *>(manager_storage.manager->call_target(manager_storage, typeid(T)));
        }

        template <typename T>
        const T * target() const noexcept {
            return static_cast<const T *>(manager_storage.manager->call_target(manager_storage, typeid(T)));
        }

#endif

        operator bool() const noexcept {

#if !ASD_OPTION_EXCEPTIONS
            return call != nullptr;
#else
            return call != &asd::detail::empty_call<Result, Arguments...>;
#endif
        }

    private:
        asd::detail::move_only_manager_storage_type manager_storage;
        Result (* call)(const asd::detail::functor_padding &, Arguments...);

        template <typename T, typename Allocator>
        void initialize(T functor, Allocator && allocator) {
            call = &asd::detail::function_manager_inplace_specialization<T, Allocator>::template call<Result, Arguments...>;
            asd::detail::setup_manager<T, Allocator>(manager_storage, std::forward<Allocator>(allocator));
            asd::detail::function_manager_inplace_specialization<T, Allocator>::store_functor(manager_storage, std::forward<T>(functor));
        }

        typedef Result(* Empty_Function_Type)(Arguments...);

        void initialize_empty() noexcept {
            typedef std::allocator<Empty_Function_Type> Allocator;
            static_assert(asd::detail::is_inplace_allocated_v<Empty_Function_Type, Allocator>, "The empty function should benefit from small functor optimization");

            asd::detail::setup_manager<Empty_Function_Type, Allocator>(manager_storage, Allocator());
            asd::detail::function_manager_inplace_specialization<Empty_Function_Type, Allocator>::store_functor(manager_storage, nullptr);
#if !ASD_OPTION_EXCEPTIONS
            call = nullptr;
#else
            call = &asd::detail::empty_call<Result, Arguments...>;
#endif
        }
    };

    template <typename T>
    bool operator ==(std::nullptr_t, const function<T> & rhs) noexcept {
        return !rhs;
    }

    template <typename T>
    bool operator ==(const function<T> & lhs, std::nullptr_t) noexcept {
        return !lhs;
    }

    template <typename T>
    bool operator !=(std::nullptr_t, const function<T> & rhs) noexcept {
        return rhs;
    }

    template <typename T>
    bool operator !=(const function<T> & lhs, std::nullptr_t) noexcept {
        return lhs;
    }

    template <typename T>
    void swap(function<T> & lhs, function<T> & rhs) {
        lhs.swap(rhs);
    }

    template <typename T>
    bool operator ==(std::nullptr_t, const move_only_function<T> & rhs) noexcept {
        return !rhs;
    }

    template <typename T>
    bool operator ==(const move_only_function<T> & lhs, std::nullptr_t) noexcept {
        return !lhs;
    }

    template <typename T>
    bool operator !=(std::nullptr_t, const move_only_function<T> & rhs) noexcept {
        return rhs;
    }

    template <typename T>
    bool operator !=(const move_only_function<T> & lhs, std::nullptr_t) noexcept {
        return lhs;
    }

    template <typename T>
    void swap(move_only_function<T> & lhs, move_only_function<T> & rhs) {
        lhs.swap(rhs);
    }
}

namespace std
{
    template <typename Result, typename... Arguments, typename Allocator>
    struct uses_allocator<asd::function<Result(Arguments...)>, Allocator>
        : std::true_type
    {
    };

    template <typename Result, typename... Arguments, typename Allocator>
    struct uses_allocator<asd::move_only_function<Result(Arguments...)>, Allocator>
        : std::true_type
    {
    };
}

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
