//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <concepts>
#include <algorithm>

//---------------------------------------------------------------------------

namespace asd::container
{
    template <class T>
    concept iterable = requires() {
        typename T::iterator;
    };

    template <iterable container>
    auto erase(container & c, size_t pos) {
        return c.erase(std::next(c.begin(), pos));
    }

    template <iterable container>
    auto erase(container & c, size_t pos, size_t count) {
        auto i = std::next(c.begin(), pos);
        return c.erase(i, i + count);
    }

    template <iterable container, typename T>
    auto try_erase(container & c, const T & value) {
        return c.erase(std::remove(c.begin(), c.end(), value), c.end());
    }

    template <class T>
    class reversed
    {
    public:
        reversed(T & container) : _container(container) {}

        auto begin() {
            return _container.rbegin();
        }

        auto end() {
            return _container.rend();
        }

        auto cbegin() {
            return _container.crbegin();
        }

        auto cend() {
            return _container.crend();
        }

        auto rbegin() {
            return _container.begin();
        }

        auto rend() {
            return _container.end();
        }

        auto crbegin() {
            return _container.cbegin();
        }

        auto crend() {
            return _container.cend();
        }

    private:
        T & _container;
    };

    template <class T>
    reversed<T> reverse(T & c) {
        return {c};
    }
}
