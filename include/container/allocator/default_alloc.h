//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <cstdint>
#include <new>

//---------------------------------------------------------------------------

namespace asd
{
    struct default_alloc
    {
        static inline void * operator new(size_t size) {
            return ::operator new(size);
        }

        static inline void operator delete(void * ptr, size_t size) {
            ::operator delete (ptr);
        }

        static inline void * operator new(size_t size, const std::nothrow_t & nt) noexcept {
            return ::operator new (size);
        }

        static inline void operator delete(void * ptr, size_t size, const std::nothrow_t & nt) noexcept {
            ::operator delete (ptr);
        }

        static inline void * operator new(size_t, void * place) noexcept {
            return place;
        }

        static inline void operator delete(void *, void *) noexcept {}
    };
}
